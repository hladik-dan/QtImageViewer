#include <QApplication>
#include "mainwindow.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    app.setApplicationName("QtImageViewer");
    app.setOrganizationName("QtImageViewer");

    MainWindow mainWindow;
    mainWindow.show();

    return (app.exec());
}
