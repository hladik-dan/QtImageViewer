#include "settingsdialog.h"
#include "ui_settingsdialog.h"

SettingsDialog::SettingsDialog(QWidget* parent)
    : QDialog(parent)
    , m_ui(new Ui::SettingsDialog())
{
    this->load();

    m_ui->setupUi(this);
    this->setupUi();
}

SettingsDialog::~SettingsDialog()
{
    delete m_ui;
}

void SettingsDialog::load()
{
    m_zoomStepValue = m_settings.value("zoomStep", 25).toInt();
    m_viewBackgroundColor =
        m_settings.value("viewBackgroundColor", "#ffffff").toString();
    m_slideshowBackgroundColor =
        m_settings.value("slideshowBackgroundColor", "#ffffff").toString();
}

void SettingsDialog::save()
{
    m_settings.setValue("zoomStep", m_zoomStepValue);
    m_settings.setValue("viewBackgroundColor", m_viewBackgroundColor);
    m_settings.setValue("slideshowBackgroundColor", m_slideshowBackgroundColor);
}

void SettingsDialog::setupUi()
{
    m_ui->zoomStepLineEdit->setFixedWidth(40);
    m_ui->zoomStepLineEdit->setValidator(new QIntValidator(1, 100, this));
    m_ui->zoomStepLineEdit->setText(QString::number(m_zoomStepValue));
    m_ui->zoomStepSlider->setValue(m_zoomStepValue);

    m_ui->viewBgColor->setStyleSheet(
        "QPushButton {"
            "background-color: " + m_viewBackgroundColor + ";"
            "border: 0px; }");

    m_ui->slideshowBgColor->setStyleSheet(
        "QPushButton {"
            "background-color: " + m_slideshowBackgroundColor + ";"
            "border: 0px; }");
}

void SettingsDialog::zoomStepValueChanged(int value)
{
    m_zoomStepValue = value;
    m_ui->zoomStepLineEdit->setText(QString::number(m_zoomStepValue));
}

void SettingsDialog::zoomStepValueChanged(QString value)
{
    m_zoomStepValue = value.toInt();
    m_ui->zoomStepSlider->setValue(m_zoomStepValue);
}

void SettingsDialog::pickViewBackgroundColor()
{
    QColor color = QColorDialog::getColor(QColor(m_viewBackgroundColor), this);
    if (!color.isValid()) {
        return;
    }

    m_viewBackgroundColor = color.name();
    m_ui->viewBgColor->setStyleSheet(
        "QPushButton {"
            "background-color: " + m_viewBackgroundColor + ";"
            "border: 0px; }");
}

void SettingsDialog::pickSlideshowBackgroundColor()
{
    QColor color =
        QColorDialog::getColor(QColor(m_slideshowBackgroundColor), this);
    if (!color.isValid()) {
        return;
    }

    m_slideshowBackgroundColor = color.name();
    m_ui->slideshowBgColor->setStyleSheet(
        "QPushButton {"
            "background-color: " + m_slideshowBackgroundColor + ";"
            "border: 0px; }");
}
