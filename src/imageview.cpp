#include "imageview.h"

#include "file.h"

ImageView::ImageView(QWidget* parent)
    : QGraphicsView(parent)
    , m_scene(new QGraphicsScene(this))
{
    this->setupUi();
}

ImageView::~ImageView()
{
}

bool ImageView::isZoomed()
{
    return (m_zoomed);
}

void ImageView::setZoomStep(int value)
{
    m_zoomIn = (100 + (double) value) / 100;
    m_zoomOut = 1 / ((100 + (double) value) / 100);
}

void ImageView::setBackground(QString value)
{
    m_scene->setBackgroundBrush(QBrush(QColor(value)));
}

void ImageView::loadFile(QString fileName)
{
    if (fileName.isNull()) {
        return;
    }

    QImage image;
    if (!image.load(fileName)) {
        return;
    }

    QPixmap pixmap;
    if (!pixmap.convertFromImage(image)) {
        return;
    }

    m_fileName = fileName;
    m_scene->clear();
    m_scene->addPixmap(pixmap);
    m_scene->setSceneRect(QRect(QPoint(0, 0), image.size()));

    fitImageToWindow();
}

void ImageView::nextImage()
{
    loadFile(File(m_fileName).getNext());
}

void ImageView::previousImage()
{
    loadFile(File(m_fileName).getPrevious());
}

void ImageView::zoomIn()
{
    m_zoomed = true;
    emit zoomed(false);
    scale(m_zoomIn, m_zoomIn);
}

void ImageView::zoomOut()
{
    m_zoomed = true;
    emit zoomed(false);
    scale(m_zoomOut, m_zoomOut);
}

void ImageView::defaultImageSize()
{
    m_zoomed = true;
    scale(1 / transform().m11(), 1 / transform().m11());
}

void ImageView::fitImageToWindow()
{
    m_zoomed = false;
    fitInView(m_scene->sceneRect(), Qt::KeepAspectRatio);
}

void ImageView::setupUi()
{
    setScene(m_scene);
}

void ImageView::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton) {
        setDragMode(QGraphicsView::ScrollHandDrag);
    }

    QGraphicsView::mousePressEvent(event);
}

void ImageView::mouseReleaseEvent(QMouseEvent* event)
{
    if (event->button() == Qt::LeftButton) {
        setDragMode(QGraphicsView::NoDrag);
    }

    QGraphicsView::mouseReleaseEvent(event);
}

void ImageView::wheelEvent(QWheelEvent* event)
{
    if (event->delta() > 0) {
        zoomIn();
    } else if (event->delta() < 0) {
        zoomOut();
    }
}
