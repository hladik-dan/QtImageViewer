#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "settingsdialog.h"

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , m_ui(new Ui::MainWindow())
    , m_settingsDialog(new SettingsDialog(this))
    , m_timer(new QTimer(this))
{
    m_ui->setupUi(this);
    this->setupUi();
}

MainWindow::~MainWindow()
{
    delete m_ui;
}

void MainWindow::setupUi()
{
    setSettings();
    setSize();
    setPosition();

    m_ui->imageView->installEventFilter(this);

    connect(m_timer, &QTimer::timeout,
        this, &MainWindow::slideshowStep);
    connect(m_ui->imageView, &ImageView::zoomed,
    m_ui->actionFitImage, &QAction::setChecked);
}

void MainWindow::setPosition()
{
    QRect screen = QApplication::desktop()->screenGeometry();
    QRect app = geometry();
    int x = static_cast<int>((double) screen.width() / 2 - app.width() / 2);
    int y = static_cast<int>((double) screen.height() / 2 - app.height() / 2);
    move(x + screen.left(), y + screen.top());
}

void MainWindow::setSettings()
{
    m_ui->imageView->setZoomStep(m_settings.value("zoomStep", 25).toInt());
    m_ui->imageView->setBackground(
        m_settings.value("viewBackgroundColor").toString());
}

void MainWindow::setSize()
{
    QRect screen = QApplication::desktop()->screenGeometry();
    int width = static_cast<int>((double) screen.width() / 100 * 30);
    int height = static_cast<int>((double) screen.height() / 100 * 30);
    resize(width, height);
}

void MainWindow::slideshowStep()
{
    m_ui->actionFitImage->setChecked(true);
    m_ui->imageView->fitImageToWindow();
    m_ui->imageView->nextImage();
}

bool MainWindow::eventFilter(QObject* watched, QEvent* event)
{
    if (event->type() == QEvent::KeyPress) {
        keyPressEvent(reinterpret_cast<QKeyEvent*>(event));
        return (true);
    }

    return (QMainWindow::eventFilter(watched, event));
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
    switch (event->key()) {
    case Qt::Key_F5:
        m_ui->actionSlideshow->trigger();
        break;
    case Qt::Key_F11:
        toggleFullScreen();
        break;
    case Qt::Key_Left:
        m_ui->imageView->previousImage();
        break;
    case Qt::Key_Right:
        m_ui->imageView->nextImage();
        break;
    case Qt::Key_Plus:
        m_ui->imageView->zoomIn();
        break;
    case Qt::Key_Minus:
        m_ui->imageView->zoomOut();
        break;
    default:
        break;
    }
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
    if (m_ui->actionFitImage->isChecked()) {
        m_ui->imageView->fitImageToWindow();
    }

    QMainWindow::resizeEvent(event);
}

void MainWindow::openFile()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Open Image",
        QString(), "Images (*.bmp *.jpg *.png)");

    if (fileName.isNull()) {
        return;
    }

    m_ui->imageView->loadFile(fileName);
    m_ui->actionFitImage->setChecked(true);
}

void MainWindow::openSettings()
{
    if (m_settingsDialog->exec()) {
        m_settingsDialog->save();
        setSettings();
    }
}

void MainWindow::toggleFitImage()
{
    if (m_ui->imageView->isZoomed()) {
        m_ui->imageView->fitImageToWindow();
    } else {
        m_ui->imageView->defaultImageSize();
    }
}

void MainWindow::toggleFullScreen()
{
    if (!isFullScreen()) {
        m_ui->mainToolBar->hide();
        showFullScreen();
    } else {
        showNormal();
        m_ui->mainToolBar->show();
    }
}

void MainWindow::toggleSlideshow()
{
    if (m_ui->actionSlideshow->isChecked()) {
        m_ui->actionFitImage->setChecked(true);
        m_ui->imageView->fitImageToWindow();
        m_ui->imageView->setBackground(
            m_settings.value("slideshowBackgroundColor").toString());

        m_timer->start(5000);
    } else {
        m_timer->stop();

        m_ui->imageView->setBackground(
            m_settings.value("viewBackgroundColor").toString());
    }
}
