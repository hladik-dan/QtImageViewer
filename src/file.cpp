#include "file.h"

File::File(QString fileName)
    : m_fileName(fileName)
{
    this->setup();
}

File::~File()
{
}

QString File::getNext()
{
    if (m_index + 1 >= entryInfoList().size()) {
        return (QString());
    }

    return (entryInfoList().at(++m_index).absoluteFilePath());
}

QString File::getPrevious()
{
    if (m_index - 1 < 0) {
        return (QString());
    }

    return (entryInfoList().at(--m_index).absoluteFilePath());
}

void File::setup()
{
    QFileInfo fileInfo(m_fileName);

    setFilter(QDir::Files | QDir::NoDotAndDotDot);
    setNameFilters(QStringList({"*.bmp", "*.jpg", "*.png"}));
    setSorting(QDir::Name);
    setPath(fileInfo.absolutePath());

    m_index = entryInfoList().indexOf(fileInfo);
}
