TARGET = QtImageViewer
TEMPLATE = app
CONFIG += c++11
QT += core gui widgets
INCLUDEPATH = include

RESOURCES += res/resources.qrc
FORMS += forms/mainwindow.ui \
         forms/settingsdialog.ui
HEADERS += include/file.h \
           include/imageview.h \
           include/mainwindow.h \
           include/settingsdialog.h
SOURCES += src/file.cpp \
           src/imageview.cpp \
           src/main.cpp \
           src/mainwindow.cpp \
           src/settingsdialog.cpp
