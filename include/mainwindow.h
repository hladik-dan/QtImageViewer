#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDesktopWidget>
#include <QEvent>
#include <QFileDialog>
#include <QKeyEvent>
#include <QMainWindow>
#include <QObject>
#include <QResizeEvent>
#include <QSettings>
#include <QString>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class SettingsDialog;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

private:
    void setupUi();
    void setPosition();
    void setSettings();
    void setSize();

    void slideshowStep();

    bool eventFilter(QObject *watched, QEvent *event);
    void keyPressEvent(QKeyEvent* event);
    void resizeEvent(QResizeEvent* event);

    Ui::MainWindow* m_ui;
    SettingsDialog* m_settingsDialog;
    QSettings m_settings;
    QTimer* m_timer;

private slots:
    void openFile();
    void openSettings();
    void toggleFitImage();
    void toggleFullScreen();
    void toggleSlideshow();
};

#endif // MAINWINDOW_H
