#ifndef FILE_H
#define FILE_H

#include <QDir>
#include <QFileInfo>
#include <QStringList>

class File : public QDir
{
public:
    explicit File(QString fileName);
    ~File();

    QString getNext();
    QString getPrevious();

private:
    void setup();

    int m_index;
    QString m_fileName;
};

#endif // FILE_H
