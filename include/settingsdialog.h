#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QColorDialog>
#include <QDialog>
#include <QDebug>
#include <QIntValidator>
#include <QSettings>
#include <QString>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget* parent = 0);
    ~SettingsDialog();

    void load();
    void save();

private:
    void setupUi();

    int m_zoomStepValue;
    QString m_viewBackgroundColor;
    QString m_slideshowBackgroundColor;
    QSettings m_settings;
    Ui::SettingsDialog* m_ui;

private slots:
    void zoomStepValueChanged(int value);
    void zoomStepValueChanged(QString value);
    void pickViewBackgroundColor();
    void pickSlideshowBackgroundColor();
};

#endif // SETTINGSDIALOG_H
