#ifndef IMAGE_H
#define IMAGE_H

#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QImage>
#include <QMouseEvent>
#include <QPixmap>
#include <QPoint>
#include <QRect>
#include <QString>
#include <QWheelEvent>

class ImageView : public QGraphicsView
{
    Q_OBJECT

public:
    explicit ImageView(QWidget* parent = 0);
    ~ImageView();

    bool isZoomed();

    void setZoomStep(int value);
    void setBackground(QString value);

    void loadFile(QString fileName);
    void nextImage();
    void previousImage();
    void zoomIn();
    void zoomOut();

signals:
    void zoomed(bool checked);

public slots:
    void defaultImageSize();
    void fitImageToWindow();

private:
    void setupUi();

    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void wheelEvent(QWheelEvent* event);

    bool m_zoomed;
    double m_zoomIn;
    double m_zoomOut;
    QGraphicsPixmapItem* m_pixmapItem;
    QGraphicsScene* m_scene;
    QString m_fileName;
};

#endif // IMAGE_H
